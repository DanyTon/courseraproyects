const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const TockenSchema = new Schema({
  _userId: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'Usuario' },
  token: { type: String, require: true},
  createDat: { type: Date, required: true, default: Date.now, expires: 43200 }
});

module.exports = mongoose.model('Token', TockenSchema);