var mongoose = require('mongoose');

var bicicletaShema = new mongoose.Schema({
	code: Number,
	color: String,
	modelo: String,
	ubicacion: {
		type: [Number], index: { type: '2dsphere', sparse: true }
	}
});

//Crea instancia del Schema
bicicletaShema.statics.createInstance = function(code, color, modelo,ubicacion) {
	return new this({
		code: code,
		color: color,
		modelo: modelo,
		ubicacion: ubicacion
	});
};

// Manejar objeto como un string
bicicletaShema.methods.toString = function() {
	return 'code: ' + this.code + '| color: ' + this.color;
};

//AllBicis
bicicletaShema.statics.allBicis = function(cb) {
	return this.find({}, cb);
};

//Add
bicicletaShema.statics.add = function(aBici, cb) {
	this.create(aBici, cb);
};

//findByCode
bicicletaShema.statics.findByCode = function(aCode, cb) {
	return this.findOne({ code: aCode }, cb);
};

//removeByCode
bicicletaShema.statics.removeByCode = function(aCode, cb) {
	return this.deleteOne({ code: aCode }, cb);
};

module.exports = mongoose.model('Bicicletas', bicicletaShema);
