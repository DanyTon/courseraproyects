var mongoose = require('mongoose');
var Reserva = require('./reserva');
const crypto = require('crypto');
const Token = require('../models/token');
const mailer =('../mailer/mailer');
const uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
const saltRounds = 10;

const validateEmail = function(email) {
  const re = /^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$/;
  return re.test(email);
}
//Autenticación y validación
var usuarioSchema = new Schema({
  nombre: {
    type: String,
    trin: true,
    required: [true, "El nombre es obligatorio"],
  },
  email: {
    type: String,
    trin: true,
    required: [true, "El email es obligatorio"],
    lowercase: true,
    unique: true,
    validate: [validateEmail, "Por favor, ingrese un email valido!!"],
    match: [
      /^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$/,
    ],
  },
  password: {
    type: String,
    required: [true, "El password es obligatorio"],
  },
  passwordResetToken: String,
  passwordResetTokenExpires: Date,
  verificado :{
    type: Boolean,
    default: false
  },
  googleId: String,
  facebookId: String
});

//Verificación de existencia
usuarioSchema.plugin(uniqueValidator, { message: 'El {PATH} ya existe con otro usuario' });
//ResertPassword
usuarioSchema.methods.resertPassword = (cb) =>{
  const token = new Token( { _userId: this.id, token: crypto.randomBytes(16).toString('hex') });
  const email_destination = this.email;
  token.save(function(err) {
    if(err) { return cb(err); }
    const mailOptions = {
      from: 'no-replay@redbicicletas.com',
      to: email_destination,
      subject: 'Reseteo de password de cuenta',
      text:'Hola, \n\n' + 'Por favor, para resetear el password de tu cuenta haz click en este link: \n' + 'http://localhost:3000' + '\/resetPassword\/' + token.token + '.\n',
    };
    mailer.sendMAil(mailOptions, (err) => {
      if(err) { return cb(err); }
      console.log('Se envió email para resetear el password a: ' + email_destination);
    });
    cb(null);
  });
};
//Create User Instance
usuarioSchema.statics.createInstance = function (nombre, email, password) {
  return new this({
    nombre: nombre,
    email: email,
    password: password,
  });
};

//Validaciòn, actualizaciòn y cifrado de password
usuarioSchema.pre('save', function(next){
  if(this.isModified('password')){
    this.password = bcrypt.hashSync(this.password, saltRounds);
  }
  next();
});
//Send static email
usuarioSchema.statics.add = function (usuario, cb) {
usuario.enviar_email_bienvenida((err, succes) => {
    if (err) console.log(err);
    console.log(succes);
  });
  return this.create(usuario, cb);
};
//Comparación
usuarioSchema.methods.validatePassword = function(password){
  return bcrypt.compareSync(password, this.password);
};
usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb) {
  var reserva = new Reserva({ usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta });
  console.log(reserva);
  reserva.save(cb);
}
//Send Email to new Users
usuarioSchema.methods.enviar_email_bienvenida = (cb) => {
  const token = new Token ( { _userId: this._id, token: crypto.randomBytes(16).toString('hex') });
  const email_destination= this.email;
    token.save(function(err) {
    if(err) return console.log(err.message); 

    const mailOptions = {
      from: 'no-replay@redbicicletas.com',
      to: email_destination,
      subject: 'Verificación de cuenta',
      text:'Hola, \n\n' + 'Por favor, verifica tu cuenta haciendo click en este link: \n' + 'http://localhost:3000' + '\/token/confirmation\/' + token.token + '.\n',
    };

usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condiction, callback) {
  const self = this;
  console.log(condiction);
  self.findOne({
    $or:[
      { 'googleId': condiction.id }, {'email': condiction.emails[0].value}
    ]
  }, (err, result) => {
    if(result) {
      callback(err, result);
    }else {
      console.log('----------CONDICTION----------');
      console.log(condiction);
      let values ={};
      values.googleId = condiction.id;
      values.email = condiction.emails[0].value;
      values.nombre = condiction.displayName || 'SIN NOMBRE';
      values.verificado = false,
      values.password = condiction._json.etag;
      console.log('----------Values----------');
      console.log(values);
      self.create(values, (err, result) => {
        if(err) { console.log(err); }
        return callback(err, result);
      });
    }
  });
}

/*usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(condiction, callback) {
  const self = this;
  console.log(condiction);
  self.findOne({
    $or:[
      { 'facebookId': condiction.id }, {'email': condiction.emails[0].value}
    ]
  }, (err, result) => {
    if(result) {
      callback(err, result);
    }else {
      console.log('----------CONDICTION----------');
      console.log(condiction);
      let values ={};
      values.facebookId = condiction.id;
      values.email = condiction.emails[0].value;
      values.nombre = condiction.displayName || 'SIN NOMBRE';
      values.verificado = false,
      values.password = crypto.randomBytes(16).toString('hex');
      console.log('----------Values----------');
      console.log(values);
      self.create(values, (err, result) => {
        if(err) { console.log(err); }
        return callback(err, result);
      });
    }
  });
}*/
    mailer.sedMail(mailOptions, function(err) {
      if(err) { return console.log(err.message) }

      console.log('A verificación email has been sent to: ' + email_destination + '.');
    });
  });
};

module.exports = mongoose.model('Usuario', usuarioSchema);