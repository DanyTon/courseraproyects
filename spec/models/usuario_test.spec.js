var mongoose = require("mongoose");
var Usuario = require('../../models/usuario');
var Reserva = require('../../models/reserva');
const bicicletas = require("../../models/bicicletas");

//Antes de ejecutarse el test
describe('Testing Bicicletas \n', function() {
  beforeEach(function(done){
    var mongoDB = 'mongodb://localhost/testUser';
    mongoose.connect(mongoDB, { useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true });

    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error: '));
    db.once('open', function(){
      console.log('\n We are connected to test database! \n');
      done();
    });
  });

  // Después de ejecutarse el test
  afterEach(function(done){
    Reserva.deleteMany({}, function(err, success){
      if (err) 
      console.log(err);
      Usuario.deleteMany({}, function(err, success) {
        if (err) 
        console.log(err);
        bicicletas.deleteMany({}, function(err, success){
          if (err) 
          console.log(err);
          done();
        });
      });
    });
  });

  describe('Cuando un usuario reserva una bici \n', () => {
    it('debe existir la reserva', (done) => {
      const usuario = new Usuario ({ nombre: 'Daniela' });
      usuario.save();
      const bicicleta = new bicicletas ({ code: 4, color: "Turquesa", modelo: "Montain", lat: 11.3795765, lng: -72.2425024 });
      bicicleta.save();

      var hoy = new Date();
      var manana = new Date();
      manana.setDate(hoy.getDate() + 1);
      usuario.reservar(bicicleta.id, hoy, manana, function(err, reserva){
        Reserva.find({}).populate('bicicleta').populate('usuario').populate('days').exec(function(err, reservas){
          console.log('Esto es la reserva \n' + reserva);
          console.log('Esta es la bicicleta: ' + bicicleta);
          console.log('Estos son los días reservados: ' + reserva.diasDeReserva());
          console.log('Usuario: ' + usuario._id + '\n' + 'Usuario_reserva: ' + reserva.usuario + '\n' + 'El id del usuario debe ser el mismo');
          expect(usuario._id).toEqual(reserva.usuario);
          expect(reserva.diasDeReserva()).toBe(2);
          expect(bicicleta.code).toBe(4);
          expect(usuario.nombre).toBe('Daniela');
          done();
        });
      });
    });
  });

});

