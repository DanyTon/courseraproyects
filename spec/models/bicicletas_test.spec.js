var mongoose = require('mongoose');
var bicicleta = require("../../models/bicicletas");


//Antes de ejecutarse el test
describe('Testing Bicicletas \n', function() {
  beforeEach(function(done){
    var mongoDB = 'mongodb://localhost/testdb';
    mongoose.connect(mongoDB, { useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true });

    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error: '));
    db.once('open', function(){
      console.log('\n We are connected to test database! \n');
      done();
    });
  });

  // Después de ejecutarse el test
  afterEach(function(done){
    bicicleta.deleteMany({}, function(err,success){
      if (err) 
      console.log(err);
      done();
    });
  });

  describe('Bicicletas.createInstance \n', () => {
    it('Crea una instancia de bicicleta \n', () => {
      var bici = bicicleta.createInstance(1, "verde", "Urbana", [11.3791334, -72.2429457]);

      expect(bici.code).toBe(1);
      expect(bici.color).toBe('verde');
      expect(bici.modelo).toBe('Urbana');
      expect(bici.ubicacion[0]).toBe(11.3791334);
      expect(bici.ubicacion[1]).toBe(-72.2429457);
    });
  });

  describe('Bicicletas.allBicis \n', () => {
    it('comienza vacía \n', (done) => {
      bicicleta.allBicis(function(err, bicis) {
        expect(bicis.length).toBe(0);

        done();
      });
    });
  });

  describe('Bicicletas.add \n', () => {
    it('agrega solo una bicicleta \n', (done) => {
      var aBici = new bicicleta( { code: 1, color: "verde", modelo: "Urbana" });
      bicicleta.add(aBici, function(err, newBici) {
        if(err)
        console.log(err);
        bicicleta.allBicis(function(err,bicis) {
          expect(bicis.length).toEqual(1);
          expect(bicis[0].code).toEqual(aBici.code)

          done();
        });
      });
    });
  });

  describe('Bicicletas.findByCode \n', () => {
    it('debe devolver la bicicleta con code 1 \n', (done) => {
      bicicleta.allBicis(function(err,bicis) {
        expect(bicis.length).toBe(0);

        var aBici = new bicicleta({ code: 1, color: "verde", modelo: "Urbana" });
        bicicleta.add(aBici, function(err, newBici) {
          if(err)
          console.log(err);

          var aBici_2 = new bicicleta({ code: 2, color: "rojo", modelo: "Montaña" });
          bicicleta.add(aBici_2, function(err, newBici_2) {
            if(err)
            console,console.log(err);
            bicicleta.findByCode(1, function(error) {
              expect(newBici.code).toBe(aBici.code);
              console.log(newBici.code)
              expect(newBici.color).toBe(aBici.color);
              console.log(newBici.color)
              expect(newBici.modelo).toBe(aBici.modelo);
              console.log(newBici.modelo)
              done();
            });
          });
        });
      });
    });
  });
});

/* describe('Bicicleta.removeCode \n', () => {
    it('debe remover la bicicleta con code 2 \n', (done) => {
      bicicleta.allBicis(function(err,bicis) {
        expect(bicis.length).toBe(0);

        var aBici = new bicicleta({ code: 1, color: "verde", modelo: "Urbana" });
        bicicleta.add(aBici, function(err, newBici) {
          if(err)
          console.log(err);
          
          var aBici_2 = new bicicleta({ code: 2, color: "rojo", modelo: "Montaña" });
          bicicleta.add(aBici_2, function (err, newBici_2) {
            if (err) 
            console, 
            console.log(err);
            var removeBici = bicicleta.removeByCode(2);
            bicicleta.deleteOne(2);
            expect(aBici_2).toEqual(0);
              done();
          });
        });
      });
    });
  });
  
});

Colección
describe("bicileta.allBicis", () => {
  it("comienza con 2 bicis por defecto", () => {
    expect(bicileta.allBicis.length).toBe(2);
  });
});
//Método add
describe("bicileta.add", () => {
  it("agregamos una", () => {
    expect(bicileta.allBicis.length).toBe(2);

    var c = new bicileta(3, "Morada", "Montañas", [11.3791334, -72.2429457]);
    bicileta.add(c);

    expect(bicileta.allBicis.length).toBe(3);
    expect(bicileta.allBicis[2]).toBe(c);
  });
});
//Método findById
describe("bicileta.findById", () => {
  it("debe devolver la bici con id 4", () => {
    expect(bicileta.allBicis.length).toBe(3);
    var aBici = new bicileta(4, "verde", "urbana");
    var aBici_2 = new bicileta(5, "azul", "montaña");
    bicileta.add(aBici);
    bicileta.add(aBici_2);

    var targetBici = bicileta.findById(4);
    expect(targetBici.id).toBe(4);
    expect(targetBici.color).toBe(aBici.color);
    expect(targetBici.modelo).toBe(aBici.modelo);
    expect(bicileta.allBicis.length).toBe(5);
  });
});

describe("bicileta.removeByID", () => {
  it("debe remover la bici con id 5", () => {
    expect(bicileta.allBicis.length).toBe(5);

    var removeBici = bicileta.removeByID(5);
    bicileta.allBicis.splice(5, 1);
    expect(bicileta.allBicis.length).toBe(4);
  });
}); */
