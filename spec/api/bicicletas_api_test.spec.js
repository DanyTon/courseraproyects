var bicicleta = require("../../models/bicicletas");
var mongoose = require('mongoose');
var request = require('request');
const bicicletas = require("../../models/bicicletas");
//var server = require('../../bin/www');

var base_url = 'http://localhost:3000/api/bicicletas'

describe('Testing Bicicletas \n', function() {
  beforeEach(function(done){
    var mongoDB = 'mongodb://localhost/testAPI';
    mongoose.connect(mongoDB, { useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true });

    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error: '));
    db.once('open', function(){
      console.log('\n We are connected to test database! \n');
      done();
    });
  });

  // Después de ejecutarse el test
  afterEach(function(done){
    bicicleta.deleteMany({}, function(err,success){
      if (err) 
      console.log(err);
      done();
    });
  });

  describe('GET BICICLETAS \n', () => {
    it('Status 200 \n', (done) => {
      request.get(base_url, function(error, response, body) {
        var result = JSON.parse(body);
        expect(response.statusCode).toBe(200);
        expect(bicicleta.length).toBe(3);
        done();
      });
    });
  });

  describe("POST BICICLETAS /create \n", () => {
    it("Status 200", (done) => {
      var headers = { "Content-Type": "application/json" };
      var aBici = '{"code": 4, "color": "Turquesa", "modelo": "Montain", "lat": 11.3795765, "lng": -72.2425024}';
      request.post({
        headers: headers,
        url: base_url + '/create',
        body: aBici
      }, function(error, response, body) {
        expect(response.statusCode).toBe(200);

        var bici = JSON.parse(aBici);
        console.log(bici);
        expect(bici.color).toBe('Turquesa');
        expect(bici.modelo).toBe('Montain');
        expect(bici.lat).toBe(11.3795765);
        expect(bici.code).toBe(4);
        done();
      }
      );
    });
  });

});




/*const { allBicis } = require("../../models/bicicletas");
beforeEach(() => {
  console.log ('testeando...');
});
describe("Bicicletas API \n", () => {
  describe("GET Bicicletas", () => {
    it("status 200", () => {
      expect(bicileta.allBicis.length).toBe(2);

      var c = new bicileta(3, "Morada", "Montañas", [11.3791334, -72.2429457]);
      bicileta.add(c);
      expect(bicileta.allBicis.length).toBe(3);

      request.get("http://localhost:3000/api/bicicletas",
        (error, response, body) => {
          expect(response.statusCode).toBe(200);
        }
      );
    });
  });
});

describe('POST Bicicleta/create \n', () => {
  it('STATUS 200', (done) => {
    var headers = { "Content-Type": "application/json" };
    var aBici = '{"id": 4, "color": "Turquesa", "modelo": "Montain", "lat": 11.3795765, "lng": -72.2425024}';
    request.post({
      headers: headers,
      url: 'http://localhost:3000/api/bicicletas/create',
      body: aBici
    },function(error, response, body) {
      expect(response.statusCode).toBe(200);

      var targetBici = JSON.parse(aBici)
      expect(targetBici.id).toBe(4);
      expect(targetBici.color).toBe("Turquesa");
      done();
    }
    );
  });
});*/  