// Se pasan las coordenadas y nivel de zoom
var map = L.map('main_map').setView([11.38013, -72.2599621], 13);

// Se creal el Tile Layer 
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
}).addTo(map);

$.ajax({
    dataType : "json",
    url : "api/bicicletas",
    success : function (response) {
        console.log (response);
        response.bicicletas.forEach(function (bici) {
            L.marker(bici.ubicación, {title:bici.id}).addTo(map);
        });
    }
})