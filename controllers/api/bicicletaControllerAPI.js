var bicileta = require('../../models/bicicletas');

exports.bicicleta_list = function(req, res) {
    res.status(200).json({
        bicicletas : bicileta.allBicis
    });
    
}

exports.bicicleta_create = function(req, res) {
    var bici = new bicileta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicación = [req.body.lat, req.body.lng]
    bicileta.add(bici);

    res.status(200).json({
        bicicletas : bici
    })
}

exports.bicicleta_delete = function(req, res) {
    bicileta.removeByID(req.body.id);
    res.status(204).send();
}

exports.bicicleta_update = function (req, res) {
    var bici = bicileta.findById (req.params.id);
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];
    res.status(200).json({
        bicicletas : bici
    })
}