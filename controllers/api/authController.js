const Usuario = require('../../models/usuario');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = {
  authenticate: function(req, res, next) {
    Usuario.findOne({ email: req.body.email }, function(err, usuario){
      if(err) {
        next(err);
      }else{
        if(usuario ===null) { return res.status(400).json({ status:"error", message: "Email/Password Invalid", data: null}); }
        if(usuario != null && bcrypt.compareSync(req.body.password, usuario.password)) { usuario.save(function(err,usuario){
          const token = jwt.sign({ id: usuario._id, }, req.app.get('secretKey'), {expiresIn: '7d'});
          res.status(200).json({ message: 'Usuario Encontrado', data: { usuario: usuario, token: token }});
          });
        }else{
          res.status(401).json({ status:"error", message: "Email/Password Invalid", data: null });
        }
      }
    });
  },
  forgotPassword: function(req, res, next) {
    Usuario.findOne({ email: req.body.email }, function(err, usuario) {
      if(usuario) { return res.status(401).json({ message: "User doesn't exist", data: null});}
      usuario.resetPassword(function(err){
        if(err) { next(err); }
        res.status(401).json({ message: "Resert Password email has been send", data: null})
      });
    });
  },
/*  authFacebookToken: function(req, res, next) {
    if(req, usuario) {
      req.usuario.save().the( () => {
        const token = jwt.sign({ id: req.usuario._id, }, req.app.get('secretKey'), {expiresIn: '7d'});
          res.status(200).json({ message: 'Usuario Encontrado o creado', data: { usuario: usuario, token: token }});
          }).catch ((err) => {
            console.log(err);
            res.satutus(500).json({ message: err.message });
          });
        }else {
          res.status(401);
        }
  },*/
}
