// Importar modulo (bicicleta)
var bicileta = require('../models/bicicletas');

// Presenta un listado de bicicletas
exports.bicicleta_list = function (req, res) {
    res.render ('bicicletas/index.pug', {bicis: bicileta.allBicis});
}

// Acceder a crear una bicileta
exports.bicicleta_create_get = function (req, res) {
    res.render ('bicicletas/create.pug');
}

// Crear una bicileta
exports.bicicleta_create_post = function (req, res) {
    var bici = new bicileta (req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng]
    bicileta.add (bici);

    res.redirect ('/bicicletas');
}

// Delete bicicletas
exports.bicicleta_delete_post = function (req, res) {
    bicileta.removeByID (req.body.id);

    res.redirect ('/bicicletas');
}

// Acceder a actualizar una bicileta
exports.bicicleta_update_get = function (req, res) {
    var bici =bicileta.findById(req.params.id);
    res.render ('bicicletas/update', {bici});
}

// Crear una bicicleta
exports.bicicleta_update_post = function (req, res) {
    var bici = bicileta.findById (req.params.id);
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];

    res.redirect ('/bicicletas');
}