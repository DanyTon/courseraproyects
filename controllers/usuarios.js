const Usuario = require('../models/usuario');

module.exports = {
  list: function (req, res, next) {
    Usuario.find({}, (err, usuarios) => {
      res.render("usuarios/index", { usuarios: usuarios });
    });
  },
  update_get: function (req, res, next) {
    Usuario.findById(req.params.id, (err, usuario) => {
      res.render("usuarios/update", { errors: {}, usuario: usuario });
    });
  },
  update: function (req, res, next) {
    var update_values = { nombre: req.params.nombre };
    Usuario.findByIdAndUpdate(req.params.id, update_values , (err, usuario) => {
      if (err) {
        console.log(err);
        res.render("usuarios/update", {
          errors: err.errors,
          usuario: new Usuario({
            nombre: req.body.nombre,
            email: req.body.email,
          }),
        });
      } else { res.redirect("/usuarios");
    }
    });
  },
  create_get: function (req, res, next) {
    let usuario = Usuario.createInstance('', '', '');
    console.log(typeof usuarios);
    res.render("usuarios/create", { errors: {}, usuario: usuario });
  },
  create: function (req, res, next) {
      if (req.body.password == req.body.confirm_password) {
        res.render("usuarios/create", { 
          errors: { comfirm_pwd: { message: 'No coincide con el password ingresado'} },
          usuario: new Usuario({
            nombre: req.body.nombre,
            email: req.body.email,
          }),
        });
          console.log("No son igaules");
          return;
      }
    var usuario = Usuario.createInstance(req.body.nombre, req.body.email,req.body.password);
    Usuario.add(usuario, (err, usuario) => {
      if (err) {
        console.log(err);
        res.render("usuarios/create", {
          errors: err.errors,
          usuario: new Usuario({
            nombre: req.body.nombre,
            email: req.body.email,
          }),
        });
      } else {
      res.redirect('/usuarios');
      console.log('Welcome Message has been send');
      }
    });
  },
  delete: function(req, res, next) {
    Usuario.findByIdAndDelete(req.body.id, (err) => {
      if(err)
        next(err);
      else
        res.redirect('/usuarios');
    });
  },
};