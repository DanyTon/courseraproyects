const passport = require('passport'), localStrategy = require('passport-local').Strategy;
const Usuario = require('../models/usuario');
var GoogleStrategy = require("passport-google-oauth20").Strategy;
//var FacebookTokenStrategy = require('passport-facebook-token')


passport.use(new localStrategy (
  function(email, password, done) {
    Usuario.findOne({ email: email }, (err, usuario) => {
      if(err) return done(err);
      if(!usuario) return done(null, false, { message: 'Email no existente o incorrecto'});
      if(!usuario.validatePassword(password)) return done(null, false, { message: 'Password incorrecto'});

      return done(null, usuario);
    });
  }
));

passport.use(new GoogleStrategy({
    clientID: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: process.env.HOST+"/auth/google/callback"
  },
  function(accessToken, refreshToken, profile, cb) {
    Usuario.findOrCreateByGoogle({ googleId: profile.id }, function (err, usuario) {
      return cb(err, usuarioj);
    });
  }
));

/*passport.use(
  new FacebookTokenStrategy(
    {
      clientID: process.env.FACEBOOK_ID,
      clientSecret: process.env.FACEBOOK_SECRET,
      callbackURL: process.env.HOST + "/auth/facebook/callback",
    },
    function (accessToken, refreshToken, profile, done) {
      try {
        Usuario.findOneOrCreateByFacebook({ facebookId: profile.id }, function (
          err,
          usuario
        ) {
          if (err) console.log("err: " + err);
          return done(err, usuario);
        });
      } catch (err2) {
        console.log(err2);
        return done(err2, null);
      }
    }
  )
);*/

passport.serializeUser( (usuario, done) => {
  done(null, usuario.id);
});

passport.deserializeUser( (id, done) => {
  Usuario.findById(id, (err, usuario) => {
    done(null, usuario);
  });
});

module.exports = passport;

