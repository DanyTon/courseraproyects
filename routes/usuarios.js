var express = require('express');
var router = express.Router();
var usuariosController = require('../controllers/usuarios')

/* GET users listing. */
/*  router.get('/', function(req, res, next) {
  res.render('usuarios/index', { title: 'Usuarios' });
});*/

router.get('/', usuariosController.list);
router.get('/create', usuariosController.create_get);
router.post('/create', usuariosController.create);


module.exports = router;
